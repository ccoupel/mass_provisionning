#!/usr/bin/env ruby
                                                                    
require 'net/https'                                                 
require 'net/https'                                                 
require 'json'                                                                                                                          
require 'yaml'                                                      
require 'csv'                                                       
                                                                    
                                                                    
def get_URL(url)                                                    
  uri=URI.parse(url)                                                
  http_srv= Net::HTTP.start(uri.host, uri.port, :use_ssl => uri.scheme == 'https', :verify_mode => OpenSSL::SSL::VERIFY_NONE) do |http| 
                                                                    
    request = Net::HTTP::Get.new uri.request_uri                    
    request.basic_auth @user, @passwd                               
                                                                                                                                        
    return http.request request # Net::HTTPResponse object          
  end                                                               
end                                                                 
                                                                                                                                        
def get_service_template(template_name)                             
  response=get_URL("#{@srv}/api/service_templates")                 
  puts "looking for service #{template_name}"                       
    j_templates=JSON.parse(response.body)                                                                                               
    puts j_templates["error"]                                       
    j_templates["resources"].each do |t|                            
      template=get_URL(t["href"])                                   
      j_template=JSON.parse(template.body)                          
      puts j_templates["error"] unless j_templates["error"].nil?                                                                        
      if j_template["name"] == template_name                        
        puts "#{j_template["service_template_catalog_id"]} => #{j_template["href"]}"
        return j_template["service_template_catalog_id"],j_template["href"]
      end                                                           
    end                                                                                                                                 
  return nil                                                        
end                                                                 
                                                                    
def post_URL(url,body)                                              
puts url                                                            
  uri=URI.parse(url)                                                                                                                    
  http_srv= Net::HTTP.new(uri.host, uri.port)                       
  http_srv.use_ssl = uri.scheme == 'https'                          
  http_srv.read_timeout = 300                                       
  http_srv.verify_mode = OpenSSL::SSL::VERIFY_NONE                                                                                      
  req = Net::HTTP::Post.new(uri.request_uri, {'Content-Type' =>'application/json'})
  req.basic_auth @user, @passwd                                                                                                         
  req.body=body
  response=http_srv.request(req)                                    
  return response                                                   
end                    

def order(url, data)                                                
  body={}                                                           
  body["action"]="order"                                            
  body["resource"]=data                                             
  puts "Request: #{body.to_json}"                                   
  res=post_URL(url,body.to_json)                                    
  puts "Response #{res.code} #{res.message}: #{res.body}"
end
require 'optparse'

Options = Struct.new(:name)

class Parser
  def self.parse(options)
    args = {}

    opt_parser = OptionParser.new do |opts|
            opts.banner = "Usage V1.2: mass_create.rb [options]"

      opts.on("-sSRV", "--server=SRV", "URL to CLOUDFORMS server ") do |n|
        args["s"] = n
      end

      opts.on("-uUSER", "--user=USER", "USER Cloudforms to log in") do |n|
        args["u"] = n
      end

      opts.on("-pPASSWORD", "--pasword=PASSWORD", "PASSWORD cloudformsi for user to log in") do |n|
        args["p"] = n
      end

      opts.on("-fCSV", "--file=CSV", "path to the CSV file containing the inputs for deployment") do |n|
        args["f"] = n
        args["file_format"]="CSV"
      end

      opts.on("-jJSON", "--json=JSON", "path to the JSON file containing the inputs for deployment") do |n|
        args["j"] = n
        args["file_format"]="JSON"
      end

      opts.on("-yYAML", "--yaml=YAML", "path to the YAML file containing the inputs for deployment") do |n|
        args["y"] = n
        args["file_format"]="YAML"
      end


      opts.on("-cITEM", "--catalog_item=ITEM", "name of the colomn containing the name of the catalog item to provision") do |n|
        args["c"] = n
      end

      opts.on("-iITEM", "--item=ITEM", "name of the catalog item to provision from file") do |n|
        args["i"] = n
      end

      opts.on("-h", "--help", "Prints this help") do
        puts opts
        exit
      end
    end

    opt_parser.parse!(options)
    return args
  end
end

def order_hash(catalog_item,hash)
    @catalog_item=@forced_catalog_item||catalog_item
    puts "preparing #{@catalog_item} with #{hash}"
    t_catalog,t_url=get_service_template(@catalog_item)
    puts "  URL=#{t_url.inspect}"
    if t_url.nil?
      puts "  no item '#{@catalog_item}' found"
      exit 255
    end
    data=hash
    data["href"]=t_url
    puts data
    order("#{@srv}/api/service_catalogs/#{t_catalog}/service_templates",data)
end

def order_csv()
  file=CSV.read(@csv_file,headers: true)
  file.each do |row|
    name=@forced_catalog_item||row[@colomn_item]
    puts "starting #{name} => #{row.to_hash}"
    order_hash(name,row.to_hash)
  end
end

def order_json()
  file=File.read(@csv_file)
  json=JSON.parse(file)
  json.each do |name,row|
    puts "starting #{name} => #{row}"
    order_hash(row["catalog_item"],row["attributs"])
  end
end

def order_yaml()
  file=File.read(@csv_file)
  yaml=YAML.load_file(@csv_file)
  yaml.each do |name,row|
    puts "starting #{name} => #{row}"
    order_hash(row["catalog_item"],row["attributs"])
  end
end

begin

  options = Parser.parse (ARGV)
  @srv=options["s"]
  puts "-s is mandatory" if @srv.nil?
  @user=options["u"]
  puts "-u is mandatory" if @user.nil?
  @passwd=options["p"]
  puts "-p is mandatory" if @passwd.nil?
  @csv_file=options["f"]||options["j"]||options["y"]
  puts "-f/j/y is mandatory" if @csv_file.nil?
  Parser.parse (["-h"]) if (@csv_file.nil?||@srv.nil?||@user.nil?||@passwd.nil?)

  @colomn_item=options["c"]||"@catalog_item"
  @forced_catalog_item=nil||options["i"]

  puts "looking for catalog item from #{@srv} in #{@colomn_item}"

  order_csv if options["file_format"]=="CSV"
  order_json if options["file_format"]=="JSON"
  order_yaml if options["file_format"]=="YAML"
end

