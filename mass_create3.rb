#!/usr/bin/env ruby

require 'net/https'
require 'net/https'
require 'json'
require 'yaml'
require 'csv'


def get_URL(url)
  uri=URI.parse(url)
  http_srv= Net::HTTP.start(uri.host, uri.port, :use_ssl => uri.scheme == 'https', :verify_mode => OpenSSL::SSL::VERIFY_NONE) do |http|

    request = Net::HTTP::Get.new uri.request_uri
    request.basic_auth @user, @passwd

    return http.request request # Net::HTTPResponse object          
  end
end

def get_service_template(template_name,catalog_name=nil)
  response=get_URL("#{@srv}/api/service_templates")
  puts "looking for service '#{template_name}' in catalog '#{catalog_name}'"
    j_templates=JSON.parse(response.body)
    puts j_templates["error"]
    j_templates["resources"].each do |t|
      template=get_URL(t["href"])
      j_template=JSON.parse(template.body)
      puts " testing #{j_template["name"]}==#{template_name}"
      puts j_templates["error"] unless j_templates["error"].nil?
      if j_template["name"] == template_name
        #checking if template is in provided catalog
        unless catalog_name.nil?
          catalog=get_URL("#{@srv}/api/service_catalogs/#{j_template["service_template_catalog_id"]}")
          j_catalog=JSON.parse(catalog.body)
          puts "    is #{j_catalog["name"]}==#{catalog_name}"
          unless  (catalog_name==j_catalog["name"])
            next
          end
        end

        puts "#{j_template["service_template_catalog_id"]} => #{j_template["href"]}"
        return j_template["service_template_catalog_id"],j_template["href"]
      end
    end
  return nil

end

def post_URL(url,body)
puts url
  uri=URI.parse(url)
  http_srv= Net::HTTP.new(uri.host, uri.port)
  http_srv.use_ssl = uri.scheme == 'https'
  http_srv.read_timeout = 300
  http_srv.verify_mode = OpenSSL::SSL::VERIFY_NONE
  req = Net::HTTP::Post.new(uri.request_uri, {'Content-Type' =>'application/json'})
  req.basic_auth @user, @passwd
  req.body=body
  response=http_srv.request(req)
  return response
end

require 'optparse'

Options = Struct.new(:name)

class Parser
  def self.parse(options)
    args = {}

    opt_parser = OptionParser.new do |opts|
            opts.banner = "Usage V1.5: mass_create.rb [options]"

      opts.on("-cITEM", "--catalog_item=ITEM", "name of the colomn containing the name of the catalog item to provision") do |n|
        args["c"] = n
      end

      opts.on("-h", "--help", "Prints this help") do
        puts opts
        exit
      end

      opts.on("-iITEM", "--item=ITEM", "name of the catalog item to provision from file") do |n|
        args["i"] = n
      end

      opts.on("-lITEM", "--ltem=ITEM", "list of comma separated provisioning name to limit") do |n|
        args["l"] = n.split(",").collect(&:strip)
      end

      opts.on("-jJSON", "--json=JSON", "path to the JSON file containing the inputs for deployment") do |n|
        args["j"] = n
        args["file_format"]="JSON"
      end
      opts.on("-fCSV", "--file=CSV", "path to the CSV file containing the inputs for deployment") do |n|
        args["f"] = n
        args["file_format"]="CSV"
      end

      opts.on("-pPASSWORD", "--pasword=PASSWORD", "PASSWORD cloudformsi for user to log in") do |n|
        args["p"] = n
      end

      opts.on("-sSRV", "--server=SRV", "URL to CLOUDFORMS server ") do |n|
        args["s"] = n
      end

      opts.on("-uUSER", "--user=USER", "USER Cloudforms to log in") do |n|
        args["u"] = n
      end

      opts.on("-yYAML", "--yaml=YAML", "path to the YAML file containing the inputs for deployment") do |n|
        args["y"] = n
        args["file_format"]="YAML"
      end

      #opts.on("-IITEM", "--ID=ITEM", "ID of a previous provision request") do |n|
      #  args["I"] = n
      #end

      opts.on("-wITEM", "--wait=ITEM", "display the status of the requests with a refresh of ITEM seconds" ) do |n|
        args["w"] = n
      end
    end

    opt_parser.parse!(options)
    return args
  end
end


def order(url, data)
  body={}
  body["action"]="order"
  body["resource"]=data
  puts "Request: #{body.to_json}"
  res=post_URL(url,body.to_json)
  puts "Response #{res.code} #{res.message}: #{res.body}"
  return res
end

def order_hash(catalog_item,catalog_name,hash)
    @catalog_item=@forced_catalog_item||catalog_item
    puts "preparing #{@catalog_item} with #{hash}"
    t_catalog,t_url=get_service_template(@catalog_item,catalog_name)
    puts "  URL=#{t_url.inspect}"
    if t_url.nil?
      puts "  no item '#{@catalog_item}' found"
      exit 255
    end
    data=hash
    data["href"]=t_url
    puts data
    result=order("#{@srv}/api/service_catalogs/#{t_catalog}/service_templates",data)
    return result
end

def order_csv()
  result={}
  file=CSV.read(@csv_file,headers: true)
  file.each do |row|
    name=@forced_catalog_item||row[@colomn_item]
    puts "starting #{name} => #{row.to_hash}"
    result[name]=order_hash(name,row.to_hash)
  end
end

def order_json()
  result={}
  file=File.read(@csv_file)
  json=JSON.parse(file)
  json.each do |name,row|
    puts "***** #{name}"
    if (@limit.nil? || @limit.include?(name))
      puts "starting  => #{row}"
      result[name]=order_hash(row["catalog_item"],row["catalog_name"],row["attributs"])
    else
      puts " not requested by limit list #{@limit}"
    end
  end
end

def order_yaml()
  result={}
  file=File.read(@csv_file)
  yaml=YAML.load_file(@csv_file)
  yaml.each do |name,row|
      puts "***** #{name}"
    if (@limit.nil? || @limit.include?(name))
      puts "starting  => #{row}"
      result[name]=order_hash(row["catalog_item"],row["catalog_name"],row["attributs"])
    else
      puts " not requested by limit list #{@limit}"
    end

  end
  return result
end

begin

  options = Parser.parse (ARGV)
  @srv=options["s"]
  puts "-s is mandatory" if @srv.nil?
  @user=options["u"]
  puts "-u is mandatory" if @user.nil?
  @passwd=options["p"]
  puts "-p is mandatory" if @passwd.nil?
  @csv_file=options["f"]||options["j"]||options["y"]
  puts "-f/j/y is mandatory" if @csv_file.nil?
  Parser.parse (["-h"]) if (@csv_file.nil?||@srv.nil?||@user.nil?||@passwd.nil?)

  @colomn_item=options["c"]||"@catalog_item"
  @forced_catalog_item=nil||options["i"]
  @limit=options["l"]||nil

  puts "looking for catalog item from #{@srv} in #{@colomn_item}"

  if options["I"].nil?
    result=order_csv if options["file_format"]=="CSV"
    result=order_json if options["file_format"]=="JSON"
    result=order_yaml if options["file_format"]=="YAML"
  else
    result=options["I"].split(",") rescue []
    options["w"]=options["w"] || "10"
  end

  if options["w"].to_i>0

    while true
      puts "###############################################"
      result.each do |k,v|
        print "#{k} => #{v.code}"
        if (v.code == "200" || v.code == "401")
          stat_url=JSON.parse(v.body)["results"][0]["href"] rescue nil
          print "  URL=#{stat_url}"
          status=get_URL(stat_url)  unless stat_url.nil?
          puts "  code=#{status.code}"
          if status.code == "200"
            new_status=JSON.parse(status.body) rescue nil

            puts "  #{new_status["approval_state"]}:#{new_status["request_state"]} => #{new_status["status"]} : #{new_status["message"]}" rescue nil
          end
        end
      end
    sleep options["w"].to_i
    end
  end
end

