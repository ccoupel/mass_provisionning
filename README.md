every 10 seconds# mass_provisionning

This script is called via CLI to start a provisonning. The inputs are from a CSV/JSON or YAML file; 

Usage V1.5: mass_create.rb [options]
    -s, --server=SRV                 URL to CLOUDFORMS server 
    -u, --user=USER                  USER Cloudforms to log in
    -p, --pasword=PASSWORD           PASSWORD cloudformsi for user to log in
    -f, --file=CSV                   path to the CSV file containing the inputs for deployment
    -j, --json=JSON                  path to the JSON file containing the inputs for deployment
    -y, --yaml=YAML                  path to the YAML file containing the inputs for deployment
    -c, --catalog_item=ITEM          name of the colomn containing the name of the catalog item to provision
    -i, --item=ITEM                  name of the catalog item to provision from file
    -l, --ltem=ITEM                  list of comma separated provisioning name to limit
    -w, --wait=ITEM                  display the status of the requests with a refresh of ITEM seconds
    -h, --help                       Prints this help




ie: file.csv
This file headers are the dialog inputs names.
param_tenant_name,param_tenant_id,param_environment,stack_name
CC_test_1,x1111,DEV,CC_test_stack_1
CC_test_2,x2222,QUALIF,CC_test_stack_2
CC_test_3,x3333,PROD,CC_test_stack_3

and called:
# mass_create.rb -s https://cloudforms.domain -i add_organisation -f file.csv


ie: file.csv
This file headers are the dialog inputs names.
catalog_item,option_0_service_name,number_of_vms,isChildService,option_0_parentServiceId,tag_0_entity,tag_0_environment,tag_0_service,tag_0_os,tag_0_zone,tag_0_sous_zone,option_0_number_of_sockets,option_0_cores_per_socket,option_0_vm_memory,option_0_os_hdd_size,option_0_data_hdd_size,option_0_data_hdd_size,option_0_data_hdd_1_size
test_catalog_item,CC-test,1,f,,dig,d,n_a,linux,intra,n_a,1,1,1,50,10,10

and called:
# mass_create.rb -s https://cloudforms.domain  -f file.csv -c catalog_item




ie: file.json
{
  "AA":{
          "catalog_item":"test_item",
          "catalog_name": "my_catalog",
          "attributs":{
            "attr1":"aa",
            "attr2":"bb"
          }
  },
  "BB":{
          "catalog_item":"test_item2",
          "attributs":{
            "attr1":"xx",
            "attr2":"yy"
          }
  }
}

and called:
# mass_create.rb -s https://cloudforms.domain -i add_organisation -j file.json

ie: file.yaml
---
  AA: 
    catalog_item: test_item
    catalog_name: my_catalog
    attributs:
      attr1: aa
      attr2: bb
  BB: 
    catalog_item: test_item2
    attributs:
      attr1: xx
      attr2: yy

and called:
# mass_create.rb -s https://cloudforms.domain -i add_organisation -y file.yml

Show every 10 seconds status of the requested provision with 
# mass_create.rb -s https://cloudforms.domain -i add_organisation -y file.yml -w 10

Limit the provisionning to a list of provisioning objects (works only with YAML and JSON):
# mass_create.rb -s https://cloudforms.domain -i add_organisation -y file.yml -w 10 -l "AA,BB"